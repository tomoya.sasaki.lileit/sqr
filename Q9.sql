﻿select
 t1.category_name,
 sum(t2.item_price) AS total_price
from
 item_category t1
inner join
 item t2
on
 t1.category_id = t2.category_id
group by
 t1.category_id
order by
 total_price desc;
